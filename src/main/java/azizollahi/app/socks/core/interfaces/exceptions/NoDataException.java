package azizollahi.app.socks.core.interfaces.exceptions;

public class NoDataException extends Throwable {
	public NoDataException(String message){
		super(message);
	}
}
